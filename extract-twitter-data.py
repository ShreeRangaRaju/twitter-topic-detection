#!/usr/bin/env python

__author__ = "Shree Ranga Raju"

import os
import sys
import tweepy
import pymongo
import time
import json
from datetime import datetime
from pprint import pprint


def initTwitterAPI():
	# Twitter API Keys and secrets
	consumer_key = 	'xBtsxLlR91JCc3oQy2yEY89Vk'
	consumer_secret = 'qxnhmkqIPEttz8N3mQIlU471cE3amI2e0yOqLvQJnslAdpRh5U'
	access_token = '144670619-QZ65Sa4E0189XA1dCks8ZSte92mr5v2vfp0cDbea'
	access_secret = '6ZZ6Uj1omxUi3Tpn5e9U57dVJ1weZ2AF7KTHNIDj3zERe'

	auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_secret)

	api = tweepy.API(auth, wait_on_rate_limit = True, wait_on_rate_limit_notify = True)

	return api


def initMongoDB():
	#MongoDB instance. Port 27017 (Default Mongo port)
	client = pymongo.MongoClient("localhost:27017")
	db = client.dummydb
	print('tweets' in db.collection_names())
	if db.tweets.count() != 0:
		db.tweets.drop()

	return db


def parse_json_tweet(data):
    tweet = json.loads(data)
    date = tweet['created_at']
    tweet_id = tweet['id']
    nfollowers = tweet['user']['followers_count']
    nfriends = tweet['user']['friends_count']

    if 'retweeted_status' in tweet:
    	#print retweeted-status
        text = tweet['retweeted_status']['text']
    else:
        text = tweet['text']

    hashtags = [hashtag['text'] for hashtag in tweet['entities']['hashtags']]
    users = [user_mention['screen_name'] for user_mention in tweet['entities']['user_mentions']]
    urls = [url['expanded_url'] for url in tweet['entities']['urls']]
    
    media_urls = []
    if 'media' in tweet['entities']:
        media_urls = [media['media_url'] for media in tweet['entities']['media']]       

    return [date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends]   


def push_to_db(date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends):
	# The method strptime() parses a string representing 
	# time according to a format. The return value is a struct_time as returned by gmtime() or localtime().
	d = time.strptime(date.replace("+0000", ''), '%a %b %d %H:%M:%S %Y') 
	tweet_unixtime = int(time.mktime(d)) # changing to unix time

	db_json = dict.fromkeys(['tweet_unixtime', 'tweet_gmttime', 'tweet_id', 'tweet_text', 'hashtags',
							  'userMentions', 'urls', 'media_urls', 'nfollowers', 'nfriends'])
	db_json['tweet_unixtime'] = tweet_unixtime
	db_json['tweet_gmttime'] = date
	db_json['tweet_id'] = tweet_id
	db_json['tweet_text'] = text
	db_json['hashtags'] = hashtags
	db_json['userMentions'] = users
	db_json['urls'] = urls
	db_json['media_urls'] = media_urls
	db_json['nfollowers'] = nfollowers
	db_json['nfriends'] = nfriends

	insert_json_db = db.tweets.insert_one(json.loads(json.dumps(db_json)))


# Initialize the service with first 100 tweets from each user.
def initService(user_name):
	temp1 = []

	for tweets in tweepy.Cursor(api.user_timeline, id = user_name).items(5):
		temp1.append(tweets.id)
		data = json.dumps(tweets._json)
		[date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends] = parse_json_tweet(data)
		push_to_db(date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends)

	return temp1[0]


# Get new tweets if any based on SinceId
def getNewTweets(prevSinceId):
	temp2 = []

	for newTweets in tweepy.Cursor(api.user_timeline, since_id = prevSinceId).items():
		if newTweets:
			print newTweets.text
			temp2.append(newTweets.id)
			# newData = json.dumps(newTweets._json)
			# [date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends] = parse_json_tweet(newData)

	temp2.append([]) # This had to be done due to tweepy.Cursor limitations

	return temp2[0]
			

		
if __name__ == "__main__":

	# Twitter User Id's
	user_names = ['nytimes', 'thesun', 'thetimes', 'ap', 'cnn', 'bbcnews', 'cnet', 'msnuk', 'telegraph', 'usatoday', 
				'wsj', 'washingtonpost', 'bostonglobe', 'newscomauhq', 'skynews', 'sfgate', 'ajenglish', 'independent', 
				'guardian', 'latimes', 'reutersagency', 'abc', 'bloomberg', 'bw', 'time']

	# user_names = ['thetimes', 'nytimes', 'abc']

	api = initTwitterAPI()
	print "Initialized Twitter API." + "\n"

	db = initMongoDB()
	print "Initialized Mongodb."

	for user_name in user_names:
		initService(user_name)
		# sId = initService(user_name)
		# prevSinceId.append(sId)

	# # while(True):
	# for i in prevSinceId:
	# 	#print "Previous SinceId is " + str(i)
	# 	#yoyo = getNewTweets(i)
	# 	newSinceId.append(getNewTweets(i))

	# for i in newSinceId:
	# 	print i
	

	
	
