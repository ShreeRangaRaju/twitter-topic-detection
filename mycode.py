#! /usr/bin/env python
# -*- coding: utf-8 -*- 

import nltk
import numpy as np
import re
import pymongo
import CMUTweetTagger
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import preprocessing
from sklearn.metrics.pairwise import pairwise_distances
from scipy.cluster.hierarchy import dendrogram
# from sklearn import metrics
import scipy.cluster.hierarchy as sch
import fastcluster
from collections import Counter
from matplotlib import pyplot as plt


my_text_1 = {'84':'Donald Trump wins 2016 USA presidential elections.', 
			 '85':'Next president of United States of America (USA) is Donald Trump. He beats HIllary Clinton',
			 '86':'I called Karen Feng fake ass girl. She got mad and left. Hope She comes back someday. Im sorry Karen',
			 '87':'Apple Inc releases its New iphone version 8',
			 '88':'Hillary Clintons was defeated by Donald Trump in 2016 US elections'}

my_text_2 = {'89':'2016 USA presidential elections Winner is Donald Trump.', 
			 '90':'Donald Trump looks at his daughter Ivanka trump as a piece. No taboo in his world.',
			 '91':'Hillary Clinton looses to Donald Trump in 2016 presidential elections',
			 '92':'I asked Emily out but she did not accept my invitation and refused me politely',
			 '93':'Yoooooo whats good homie? How you doin fam?'}

my_text_3 = {'94':'Hurricane Matthew destroys almost 50 houses in southern part of USA. 45 people killed', 
			 '95':'Hurricane Matthew kills around 50 people. CNN news confirms',
			 '96':'Mr. Trump is the new president of USA. He shockingly defeats his potential opponent Hillary clinton.',
			 '97':'dogs rats Dogs',
			 '98':'dogs rats Dogs Cats'}

my_text = [my_text_1, my_text_2, my_text_3]

def load_stopwords():
	stop_words = nltk.corpus.stopwords.words('english')
	stop_words.extend(['this','that','the','might','have','been','from',
                'but','they','will','has','having','had','how','went'
                'were','why','and','still','his','her','was','its','per','cent',
                'a','able','about','across','after','all','almost','also','am','among',
                'an','and','any','are','as','at','be','because','been','but','by','can',
                'cannot','could','did','do','does','either','else','ever','every',
                'for','from','get','got','had','has','have','he','her','hers','him','his',
                'how','however','i','if','in','into','is','it','its','just','least','let',
                'like','likely','may','me','might','most','must','my','neither','nor',
                'not','of','off','often','on','only','or','other','our','own','rather','said',
                'say','says','she','should','since','so','some','than','that','the','their',
                'them','then','there','these','they','this','tis','to','too','us',
                'wants','was','we','were','what','when','where','which','while','who',
                'whom','why','will','with','would','yet','you','your','ve','re','rt', 'com',
                'retweeting', 'via', 'rt&amp', '&amp',
                '#retweet', 'retweet'])
	stop_words = set(stop_words)
	return stop_words


def normalize_text(text):
	try:
		text = text.encode('utf-8')
	except: pass
	text = re.sub('((www\.[^\s]+)|(https?://[^\s]+)|(pic\.twitter\.com/[^\s]+))','', text)
	text = re.sub('@[^\s]+','', text)
	text = re.sub('#([^\s]+)', '', text)
	text = re.sub('[:;>?<=*+()/,\-#!$%\{˜|\}\[^_\\@\]1234567890’‘]',' ', text)
	text = re.sub('[\d]','', text)
	text = text.replace(".", '')
	text = text.replace("'", ' ')
	text = text.replace("\"", ' ')
	#text = text.replace("-", " ")
	#normalize some utf8 encoding
	text = text.replace("\x9d",' ').replace("\x8c",' ')
	text = text.replace("\xa0",' ')
	text = text.replace("\xa6",' ')
	text = text.replace("\xc2",' ')
	text = text.replace("\x9d\x92", ' ').replace("\x9a\xaa\xf0\x9f\x94\xb5", ' ').replace("\xf0\x9f\x91\x8d\x87\xba\xf0\x9f\x87\xb8", ' ').replace("\x9f",' ').replace("\x91\x8d",' ')
	text = text.replace("\xf0\x9f\x87\xba\xf0\x9f\x87\xb8",' ').replace("\xf0",' ').replace('\xf0x9f','').replace("\x9f\x91\x8d",' ').replace("\x87\xba\x87\xb8",' ')	
	text = text.replace("\xe2\x80\x94",' ').replace("\x9d\xa4",' ').replace("\x96\x91",' ').replace("\xe1\x91\xac\xc9\x8c\xce\x90\xc8\xbb\xef\xbb\x89\xd4\xbc\xef\xbb\x89\xc5\xa0\xc5\xa0\xc2\xb8",' ')
	text = text.replace("\xe2\x80\x99s", " ").replace("\xe2\x80\x98", ' ').replace("\xe2\x80\x99", ' ').replace("\xe2\x80\x9c", " ").replace("\xe2\x80\x9d", " ")
	text = text.replace("\xe2\x82\xac", " ").replace("\xc2\xa3", " ").replace("\xc2\xa0", " ").replace("\xc2\xab", " ").replace("\xf0\x9f\x94\xb4", " ").replace("\xf0\x9f\x87\xba\xf0\x9f\x87\xb8\xf0\x9f", "")
	return text


def tokenizer(text):
	tokens = []
	features = []
	tokens = text.split()
	for word in tokens:
		if word.lower() not in stop_words and len(word) > 2:
			features.append(word)
	return features


def text_processor(text):
	features = []
	text = normalize_text(text)
	features = tokenizer(text)
	return features


def fancy_dendrogram(*args, **kwargs):
    max_d = kwargs.pop('max_d', None)
    if max_d and 'color_threshold' not in kwargs:
        kwargs['color_threshold'] = max_d
    annotate_above = kwargs.pop('annotate_above', 0)

    ddata = dendrogram(*args, **kwargs)

    if not kwargs.get('no_plot', False):
        plt.title('Hierarchical Clustering Dendrogram (truncated)')
        plt.xlabel('sample index or (cluster size)')
        plt.ylabel('distance')
        for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
            x = 0.5 * sum(i[1:3])
            y = d[1]
            if y > annotate_above:
                plt.plot(x, y, 'o', c=c)
                plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
                             textcoords='offset points',
                             va='top', ha='center')
        if max_d:
            plt.axhline(y=max_d, c='k')
    return ddata


if __name__ == '__main__':

	debug = 0	
	client = pymongo.MongoClient('localhost:27017')
	db = client.dummydb
	my_text_db = []
	for i in db.tweets.find():
		my_text_db.append(i['tweet_text'])
		#print i['_id']

	stop_words = load_stopwords()
	window_corpus = []
	dfVocTimeWindows = {}
	ntweets = 0
	s = 0
	for i in my_text:
		s += 1
		print "ITERATION NUMBER IS:", s
		for j in i:
			text = i[j]
			# text = i['tweet_text']
			ntweets += 1
			features = text_processor(text)
			tweet_bag = ""
			if len(features) > 3:
				for feature in features:
					tweet_bag += feature + ',' # all the words are concatenated if not comma
			window_corpus.append(tweet_bag)
		#print len(window_corpus)
		#print window_corpus
		print "Number of tweets in the window_corpus: ", ntweets
		vectorizer = CountVectorizer(min_df = 2, binary = True, ngram_range = (2,3)) # changed min_df to 2 from 1
		# testX = vectorizer.fit_transform(my_text)
		X = vectorizer.fit_transform(window_corpus).toarray() # this is document-term-matrix

		X_dense = np.matrix(X).astype('float')
		#Scale X_dense to zero mean and unit variance
		X_scaled = preprocessing.scale(X_dense)
		#Normalize the scaled samples
		X_normalized = preprocessing.normalize(X_scaled, norm = 'l2') 
		#distance matrix is sample by samples distances
		dist_matrix = pairwise_distances(X_normalized, metric = 'cosine')
		#cluster tweets
		print "Clustering tweets, fastcluster, method is average, distance metric is cosine"
		L = fastcluster.linkage(dist_matrix, method = 'average')

		vocX = vectorizer.get_feature_names() # this is vocabulary list
		
		print vocX

		# Pos tokens
		print "Generating pos tokens..." # This takes some time.
		boost_entity = {}
		pos_tokens = CMUTweetTagger.runtagger_parse([term.upper() for term in vocX])
		for i in pos_tokens:
			term = ''
			for foo in range(0,len(i)):
				term += i[foo][0].lower() + " "
			if "^" in str(i):
				boost_entity[term.strip()] = 2.5
			else:
				boost_entity[term.strip()] = 1.0 



		dfX = X.sum(axis=0)
		print "dfx:", dfX
		dfVoc = {}
		boosted_wdfVoc = {}	
		wdfVoc = {}
		keys = vocX
		vals = dfX
		for k, v in zip(keys, vals):	
			dfVoc[k] = v
		for k in dfVoc: 
 					try:
 						dfVocTimeWindows[k] += dfVoc[k]
 						avgdfVoc = (dfVocTimeWindows[k] - dfVoc[k])/(s - 1)
 						#avgdfVoc = (dfVocTimeWindows[k] - dfVoc[k])
					except:
 						dfVocTimeWindows[k] = dfVoc[k]
 						avgdfVoc = 0
 
 					wdfVoc[k] = (dfVoc[k] + 1) / (np.log(avgdfVoc + 1) + 1)
					try:
						boosted_wdfVoc[k] = wdfVoc[k] * boost_entity[k]
					except: 
						boosted_wdfVoc[k] = wdfVoc[k]
					# print boosted_wdfVoc
		

		dt =  0.7		 # dendrogram cut-off		
		print "hclust cut threshold:", dt
		indL = sch.fcluster(L, dt*dist_matrix.max(), 'distance')
		#print "indL:", indL
		freqTwCl = Counter(indL)
		print "n_clusters:", len(freqTwCl)
		print(freqTwCl.most_common())

		if debug == 1:
			fancy_dendrogram(
				L,
				truncate_mode='lastp',
				p=30,
				leaf_rotation=90.,
				leaf_font_size=12.,
				show_contracted=True,
				annotate_above=10,	
				max_d= dt*dist_matrix.max())
			plt.show()
		npindL = np.array(indL)
		print "npindL: ", npindL

		cluster_score = {}
		freq_th = 2	# this should be equal to min_df ?
		for clfreq in freqTwCl.most_common(): # most_common "here" coz it converts to tuples
			cl = clfreq[0]
			freq = clfreq[1]
			cluster_score[cl] = 0
			if freq >= freq_th:
				clidx = (npindL == cl).nonzero()[0].tolist()
				cluster_centroid = X[clidx].sum(axis=0)
				print  "Cluster Centroid:", cluster_centroid
				try:
					cluster_tweet = vectorizer.inverse_transform(cluster_centroid) 
					for term in np.nditer(cluster_tweet):
						try:
							cluster_score[cl] = max(cluster_score[cl], boosted_wdfVoc[str(term).strip()]) 
						except:
							pass
				except:
					pass
				cluster_score[cl] /= freq
			else:
				break
		# sorted_clusters = sorted()
		sorted_clusters = sorted( ((v,k) for k,v in cluster_score.iteritems()), reverse=True)
		print "Sorted Clusters:"
		print sorted_clusters

		for score, cl in sorted_clusters:
			if score >= 2.5:
				clidx = (npindL == cl).nonzero()[0].tolist()
				print "Final cluster tweets index in the present time:", clidx

		
		window_corpus = []
		ntweets = 0

