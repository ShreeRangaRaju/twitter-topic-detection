#!/usr/bin/python

import os
import sys
import tweepy
import json
import time
import codecs
from pprint import pprint

# Twitter API Keys and secrets
consumer_key = 	'xBtsxLlR91JCc3oQy2yEY89Vk'
consumer_secret = 'qxnhmkqIPEttz8N3mQIlU471cE3amI2e0yOqLvQJnslAdpRh5U'
access_token = '144670619-QZ65Sa4E0189XA1dCks8ZSte92mr5v2vfp0cDbea'
access_secret = '6ZZ6Uj1omxUi3Tpn5e9U57dVJ1weZ2AF7KTHNIDj3zERe'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

# The API class provides access to the entire twitter RESTful API methods. 
api = tweepy.API(auth, wait_on_rate_limit = True, wait_on_rate_limit_notify = True)

# Twitter User Id's
# user_names = ['nytimes', 'thesun', 'thetimes', 'ap', 'cnn', 'bbcnews', 'cnet', 'msnuk', 'telegraph', 'usatoday', 
# 			'wsj', 'washingtonpost', 'bostonglobe', 'newscomauhq', 'skynews', 'sfgate', 'ajenglish', 'independent', 
# 			'guardian', 'latimes', 'reutersagency', 'abc', 'bloomberg', 'bw', 'time']

 
def parse_json_tweet(data):
    tweet = json.loads(data)
    date = tweet['created_at']
    tweet_id = tweet['id']
    nfollowers = tweet['user']['followers_count']
    nfriends = tweet['user']['friends_count']

    if 'retweeted_status' in tweet:
        text = tweet['retweeted_status']['text']
    else:
        text = tweet['text']

    hashtags = [hashtag['text'] for hashtag in tweet['entities']['hashtags']]
    users = [user_mention['screen_name'] for user_mention in tweet['entities']['user_mentions']]
    urls = [url['expanded_url'] for url in tweet['entities']['urls']]
    
    media_urls = []
    if 'media' in tweet['entities']:
        media_urls = [media['media_url'] for media in tweet['entities']['media']]       

    return [date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends]   

user_name = 'ranga_raju'
output_file = codecs.open('data-extracted.txt', 'w', 'utf-8')

for tweets in tweepy.Cursor(api.user_timeline, id = user_name).items(10):
		data = json.dumps(tweets._json)
		[date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends] = parse_json_tweet(data)
		output_file.write(str([date, tweet_id, text, hashtags, users, urls, media_urls, nfollowers, nfriends]) + "\n")
	
output_file.close()
    
